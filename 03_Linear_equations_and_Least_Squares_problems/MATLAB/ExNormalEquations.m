% ExNormalEquations.m  -- version 2003-11-12
A=[1 1; 1 2; 1 3; 1 4];  b=[2 1 1 1]';
[m,n] = size(A);
C = A'*A; c = A'*b; bb = b'*b;
Cbar = [C c; c' bb];
Gbar = chol(Cbar)';
G = Gbar(1:2,1:2); z = Gbar(3,1:2)'; rho = Gbar(3,3);
x = G'\z
sigma2 = rho^2/(m-n)
T = trilinv(G);     % Inversion of triangular matrix
S = T'*T;
Mcov = sigma2*S
