function L = trilinv(L)
% trilinv.m  -- version 1995-03-25
for i = 1:length(L(:,1))
    for j = 1:i-1
        L(i,j) = -L(i,j:i-1)*L(j:i-1,j) / L(i,i);
    end
    L(i,i) = 1/L(i,i);
end
