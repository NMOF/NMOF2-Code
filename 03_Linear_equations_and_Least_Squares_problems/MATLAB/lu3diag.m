function [l,u] = lu3diag(p,d,q)
% lu3diag.m  -- version 2018-11-30
n = length(d); l = zeros(1,n-1); u = l;
u(1) = d(1);
for i = 2:n
    l(i-1) = p(i-1)/u(i-1);
    u(i)   = d(i) - l(i-1)*q(i-1);
end