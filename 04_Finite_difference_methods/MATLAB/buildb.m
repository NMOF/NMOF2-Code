function b = buildb(Ap,B,theta,V0,V1)
% buildb.m  -- version  2010-10-30
% Builds right-hand side vector for theta method
n = size(Ap,1);
b = zeros(n,1); f7 = 1; N = n+1;
for i = 1:n
    b(i) = Ap(i,:)*V1(f7+(i-1:i+1));
end
b(1) = b(1) + theta*B(1)*V0(f7+0) + (1-theta)*B(1)*V1(f7+0);
b(n) = b(n) + theta*B(2)*V0(f7+N) + (1-theta)*B(2)*V1(f7+N);
