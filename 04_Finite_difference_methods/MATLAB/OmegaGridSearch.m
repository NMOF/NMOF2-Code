function [w,r] = OmegaGridSearch(A,winf,wsup,npoints)
% OmegaGridSearch.m  -- version  2010-10-28
% Grid search for optimal relaxation parameter
if nargin == 1, npoints = 21;  winf = .9; wsup = 1.8; end
D = diag(diag(A)); L = tril(A,-1); U = triu(A,1);
omegavec = linspace(winf,wsup,npoints);
for k = 1:npoints
    w = omegavec(k);
    M = D + w*L;
    N = (1-w)*D - w*U;
    v = eig(inv(M)*N);
    s(k) = max(abs(v));
end
[smin,i] = min(s);
w = omegavec(i);
plot(omegavec,s,'o','MarkerSize',5,'Color',[.5 .5 .5])
set(gca,'xtick',[winf 1 w wsup]);
ylabel('\rho','Rotation',0); xlabel('\omega')
if nargout == 2, r = smin; end
