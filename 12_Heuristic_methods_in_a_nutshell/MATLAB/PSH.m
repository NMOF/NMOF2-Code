function [Gbest,xbest,FG,gbest] = PSH(PS,Data)
% PSH.m  -- version  2018-11-01
OF = PS.OF;  SP = PS.SP;
nP = PS.nP;  nG = PS.nG;  d = Data.n; FG = NaN(nG,1);
[F,P,Gbest,gbest]  = feval(SP,d,nP,OF,Data); %--Start. pop.
P = P';  Pbest = P;  Fbest = F;   v = PS.cv * rand(nP,d);
for k = 1:nG
    v = v + PS.c1* rand(nP,d).*(Pbest - P) + ...
          PS.c2* rand(nP,d).*(ones(nP,1)*Pbest(gbest,:)-P);
    v = min(v, PS.vmax);  
    v = max(v,-PS.vmax);
    P = P + v;
    for i = 1:nP
        F(i) = feval(OF,P(i,:),Data);
    end
    I = find(F < Fbest);
    if ~isempty(I)
        Fbest(I)   = F(I);
        Pbest(I,:) = P(I,:);
        [Fmin,ib]  = min(F(I));
        if Fmin < Gbest
            Gbest = Fmin; gbest = I(ib); FG(k) = Gbest;
        end
    end
end
xbest = Pbest(gbest,:);


