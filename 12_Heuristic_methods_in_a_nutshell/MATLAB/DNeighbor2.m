function x1 = DNeighbor2(x0,ign1,Data,ign2)
% DNeighbor2.m  -- version  2018-10-28
x1 = x0;  n = Data.n;
J = find(x0);   nonJ = find(x0==0);   done = 0;
while ~done
    u = rand;
    if u < 1/3
        m = numel(J);
        if m > 1
            k = unidrnd(m,1); 
            j = J(k);    x1(j) = 0;   done = 1;
        end
    elseif u < 2/3
        m = numel(nonJ);
        if m < n
            k = unidrnd(m,1);
            j = nonJ(k);  x1(j) = 1;  done = 1;
        end
    else
        m = numel(J);    k = unidrnd(m,1);
        jout = J(k);    x1(jout) = 0;
        m = numel(nonJ); k = unidrnd(m,1);
        jin = nonJ(k);  x1(jin) = 1;  done = 1;
    end
end