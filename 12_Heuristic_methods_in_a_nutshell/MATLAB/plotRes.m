% plotRes.m  -- version 2018-11-23
figure(1), subplot(211)  % plot CDF of solutions
H = cdfplot(Sol); title('Empirical CDF of solutions')
xlabel(''); ylabel(''); set(gca,'FontSize',15)
set(H,'Color',.7*[1 0 0],'LineWidth',3);
if exist('ND','var')
   figure(1), subplot(212) % plot threshold sequence
   H = cdfplot(ND); xlabel(''); ylabel(''); hold on
   plot(TA.th,TA.Percentiles,'ko','MarkerSize',8);
   set(H,'Color',.7*[1 1 1],'LineWidth',2)
   title('Emp. distr. of distances between neighbor sol.');
   set(gca,'ytick',fliplr(TA.Percentiles),'FontSize',15);
   set(gca,'xtick',fliplr(TA.th)); xlim([0 TA.th(1)]);
end
if strcmp(P,'R') % Rounds plotted
    F = FF; J = JB; nR = size(J,1);
    nC = size(F,1); nS = nC/nR; P = 'Rounds';
else   % Generations plotted
    F = FG; P = 'Generations';
end
[nC,nRe] = size(F); Fmin = min(F(:)); Fmax = max(F(:));
for r = 1:nRe
    j = num2str(mod(r-1,2)+1);
    figure(fix((r+1)/2)+1), eval(['subplot(21',j,');']);
    if strcmp(P,'Rounds')  % -- Rounds
        plot(F(:,r),'k-'),hold on, K = J(~isnan(J(:,r)),r);
        plot(K,F(K,r),'ko','MarkerSize',8,...
            'MarkerFaceColor','w');
        for k = 1:nR-1
            plot(nS*[k k],[Fmin Fmax],'k:','LineWidth',2);
        end
    else  % -- Generations
        plot(F(:,r),'ko','MarkerSize',8,...
            'MarkerFaceColor',0.7*[1 1 1]);
    end
    set(gca,'FontSize',15); xlim([0 nC]);ylim([Fmin Fmax]);
    xlabel([P,' at restart ',num2str(r)]);  grid on;
    if strcmp(P,'Rounds'), set(gca,'xtick',nS*(1:nR)); end
end
