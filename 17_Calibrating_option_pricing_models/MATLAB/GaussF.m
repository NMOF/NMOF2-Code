function phi = GaussF(x)
phi = (1/sqrt(2*pi)) * exp(-x.^2/2);
