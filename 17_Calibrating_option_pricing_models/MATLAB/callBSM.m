function C0 = callBSM(S,X,tau,r,q,v)
% callBSM.m -- version 2010-10-26
%   S     = spot
%   X     = strike
%   tau   = time to mat
%   r     = riskfree rate
%   q     = dividend yield
%   v     = variance (vol squared)
d1 = (log(S./X) + (r - q + v / 2) .* tau) ./ (sqrt(v * tau));
d2 = d1 - sqrt(v*tau);
C0 = S.*exp(-q.*tau).*normcdf(d1,0,1) - ...
     X.*exp(-r.*tau).*normcdf(d2,0,1);