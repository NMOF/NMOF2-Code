function x1 = NewtonnD(f,x1,tol,itmax)
% NewtonnD.m  -- version  2010-09-12
if nargin < 3, tol = 1e-2; itmax = 10; end
x1 = x1(:); x0 = -x1;  k = 0; 
while ~converged(x0,x1,tol)
    x0 = x1;
    F = feval(f,x0);
    b = -F; J = numJ(f,x0);
    s = J \ b;
    x1 = x0 + s;
    k = k + 1; 
    if k > itmax, error('Maxit in NewtonnD'); end
end

