function J = numJ(f,x)
% numJ.m  -- version  2010-09-16
n = numel(x); J = zeros(n,n); x = x(:);
Delta = diag(max(sqrt(eps) * abs(x),sqrt(eps)));
F = feval(f,x);
for j = 1:n
   Fd = feval(f,(x + Delta(:,j)));
   J(:,j) = (Fd - F) / Delta(j,j);
end
