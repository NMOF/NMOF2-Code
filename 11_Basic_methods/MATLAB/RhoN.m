function F = RhoN(s)
% RhoN.m  -- version 2010-11-12
d = exp(-0.5 * s.^2) ./ sqrt(2*pi);
F = Rho(s) .* d';