function [C0,deltaE,gammaE,thetaE] = EuropeanCallGreeks(...
                                     S0,X,r, T,sigma,M)
% EuropeanCallGreeks.m -- version 2010-12-28
% compute constants
f7 = 1;  dt = T/M;  v = exp(-r * dt);
u = exp(sigma*sqrt(dt));   d = 1 /u;
p = (exp(r * dt) - d)/(u - d);

% initialize asset prices at maturity (period M)
S = zeros(M + 1,1);
S(f7+0) = S0 * d^M;
for j = 1:M
    S(f7+j) = S(f7+j - 1) * u / d;
end

% initialize option values at maturity (period M)
C = max(S - X, 0);

% step back through the tree
for i = M-1:-1:0
    for j = 0:i
        C(f7+j) = v * ( p * C(f7+j + 1) + (1-p) * C(f7+j));
        S(f7+j) = S(f7+j) / d;
    end
    if i==2
      %gamma
      gammaE = ((C(2+f7)-C(1+f7)) / (S(2+f7)-S(1+f7)) - ...
                (C(1+f7)-C(0+f7)) / (S(1+f7)-S(0+f7)))/ ...
                (0.5 * (S(2+f7)-S(0+f7)));
        %theta (aux)
        thetaE = C(1+f7);
    end
    if i==1
        %delta
        deltaE = (C(1+f7) - C(0+f7)) / (S(1+f7) - S(0+f7));
    end
    if i==0
        %theta (final)
        thetaE = (thetaE - C(0+f7)) / (2 * dt);
    end
end
C0 = C(f7+0);