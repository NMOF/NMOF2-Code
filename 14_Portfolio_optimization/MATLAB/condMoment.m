% condMoment.m -- version 2010-12-31
rp = randn(10000,1); trials = 1000;

%% squared
tic, for i = 1:trials, a = rp' * rp;     end, toc
tic, for i = 1:trials, b = sum(rp .^ 2); end, toc
max(abs(a-b))

%% other exponent
e = 4;
tic
for i=1:trials
    r = rp;
    for j = 2:e, r = r .* rp; end
    a = sum(r);
end
toc
tic, for i=1:trials, b = sum(rp .^ e); end, toc
max(abs(a-b))