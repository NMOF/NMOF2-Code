% sumSquares.m  -- version  2010-12-23
trials = 1000; n = 50000; ee = randn(n,1);
tic, for i = 1:trials, z1 = sum(ee .^ 2); end, toc
tic, for i = 1:trials, z2 = ee' * ee; end, toc
tic, for i = 1:trials, z3 = dot(ee,ee); end, toc